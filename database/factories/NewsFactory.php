<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\News;
use Faker\Generator as Faker;
use App\Categories;

$factory->define(News::class, function (Faker $faker) {
    return [
        //
    	'category_id' => Categories::all()->random()->id,
    	'title' => $faker->sentence(5),
    	'discription' => $faker->text(300),
    ];
});
