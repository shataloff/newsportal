<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\News;

class Categories extends Model
{
    //
    protected $table = 'categories';
    protected $fillable = ['order','name','slug'];

    public function news(){
    	
    	return $this->hasMany('App\News','category_id');
    }
}
