<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Categories;

class News extends Model
{
    //
    protected $fillable = ['category_id','title','discription','image'];
    
    public function categories(){
    	
    	return $this->belongsTo('App\Categories','category_id');
    }

   
}
