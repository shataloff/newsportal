<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

use Carbon\Carbon;
use Validator;
use App\Categories;
use App\News;
use App\Subscribe;
use DB;

class BlocksController extends Controller
{
      
   
    // свойство которое будет отправляться в темплейт
    public $data = ['title' => ''];


    public function getBlockHeader($data = []){


        	 $this->data['page'] = 'main';

        //получаем название всех категорий
        $navs = DB::table('categories')->select('name','slug')->get();
        

        //если не пустая выводим
        if(!empty($navs)){
        		return $this->render('components/header', ["navs" =>$navs]);
        }
        else{
        	return $this->render('errors/404');
        }
        
    }

    public function getBlockBanner($data = []){

    	// получаю данные которые пришли с другого контроллера. и совмещаю с текущими
    		$this->data = array_merge($this->data, $data);

    		//сортируем и выбираем свежую новосит для вывода в баннер
    		$lastNews = News::with('categories')->OrderBy('created_at','desc')->first();

    		//обробатываем дату
    		if(!empty($lastNews)){
    		$createdAt = Carbon::parse($lastNews->created_at);
    		$dateFormat = $createdAt->format('M d Y');
    		}
    		
    		return $this->render('components/listBanner',['lastNews' => $lastNews,'dateFormat' => $dateFormat]);
    }

     public function getBlockListNews($data = []){

    	// получаю данные которые пришли с другого контроллера. и совмещаю с текущими
    		$this->data = array_merge($this->data, $data);

    		$news = News::with('categories')->OrderBy('views','desc')->paginate(3);
    		
    		return $this->render('components/listNews',['news' =>$news]);
    }

    
    public function getBlockpageNews($data = []){

    		$this->data = array_merge($this->data, $data);
    		//получем количество просмотров новости и обновляем 
    		$views = DB::table('news')->select('views')
                        ->where('id', $data['news_id'])
                        ->first();
                      
    		 if(!empty($views)){
    		 		//прибавляем просмотр
    		 		$views->views++;
    		 	DB::table('news')
                        ->where('id', $data['news_id'])
                        ->update([
                        	'views' =>$views->views,
                        ]);
    		 }
    		
    		$itemNews = News::with('categories')->where('id',$this->data['news_id'])->get();



    		if(!empty($itemNews)){
    			return $this->render('components/pageNews',['itemNews' => $itemNews]);
    		}
    		else{
    			return $this->render('errors/404');
    		}



    		
    }
    //формируем блок для отоброжения категорий

    public function getBlockCategoryPage($data = []){

        // получаю данные которые пришли с другого контроллера. и совмещаю с текущими
        $this->data = array_merge($this->data, $data);
        //получаем id категории
        $cat_id = Categories::where('slug',$this->data['category'])->select('id')->get();
         
        $cat_id = $cat_id[0]->id;


         //получаем новости по категории   
        $news = News::with('categories')->where('category_id',$cat_id)->OrderBy('views','desc')->paginate(3);
        
           if(!empty($news)) 
                return $this->render('components/listNews',['news' =>$news]);
            else
                return $this->render('errors/404');




    }
    public function getBlockSearchPage($data = []){
        
        // получаю данные которые пришли с другого контроллера. и совмещаю с текущими
        $this->data = array_merge($this->data, $data);
    
        if(!empty($this->data['searchResult'])){

            $news = News::with('categories')->where('title','like','%'.$this->data['searchResult']['result'].'%')
                                             ->orWhere('discription','like','%'.$this->data['searchResult']['result'].'%')->OrderBy('views','desc')->paginate(3);

                                             
            if(!$news->isEmpty()){

                return $this->render('components/searchNews',['news' => $news]);
            }
            else{

                return $this->render('components/searchNewsNotFound');
            }
        }                                    
    }

    //формируем футер 
    public function getBlockFooter(){


    	return $this->render('components/footer');
    }

    //обработка ajax запросов
    
    public function ajax(Request $r){
         // get method
        $method = $r->method();

        // get request
        $r = $r->all();
        if (isset($r['action'])) {
            $res = ['error' => 0, 'msg' => '', 'busy' => 0];

            // function for edit the message
            $msg = function ($msg = "") use (&$res) {
                $res['msg'] = $msg;
            };
         switch ($r['action']) {

            case 'subscribe':
                    
                    // validate fields
                    $validator = Validator::make($r, [
                        'email' => 'required|max:255|email',
                    ]);
                     // if has errors
                    if ($validator->fails()) {
                        $res['error']++;
                        // set error message
                        $msg($validator->errors()->first());
                    } // if all okay
                    else {
                        // add email to list
                        $subscribes = Subscribe::where('email',$r['email'])->get();
                        
                        if( $subscribes->isEmpty()){
                            Subscribe::create([
                            'email' => $r['email'],
                        ]);
                        $msg("Вы подписались на обновления");
                    } else{
                        
                        $subscribes =  Subscribe::find($subscribes[0]->id);
                        $subscribes->delete();
                        $msg("Вы отписались от  обновлений");
                        
                    }
                    break;
                    }
         }

    }
     return json_encode($res, JSON_UNESCAPED_UNICODE);
}



     private function render($view, $data = [])
    {
        // set data
        $data['data'] = $this->data;

        return [
            'content' => View::make("blocks/" . $view, $data)->render(),
            'data' => $this->data
        ];    }
}
