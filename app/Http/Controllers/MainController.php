<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Categories;
use App\News;

class MainController extends Controller
{
    //
	// свойство для класса BlocksController
    public $bl;

    public function __construct(){
    	 // создание екземпляра класса, в котором генерируются html блоки
        $this->bl = new BlocksController();
    }
    
    public function index(){

    		$this->data['page'] = 'main';
    		
    	
    	echo
            $this->getView('getBlockHeader') .
            $this->getView('getBlockBanner') .
            $this->getView('getBlockListNews').
            $this->getView('getBlockFooter');

    
    }
    //метод для отображение новости
    public function pageNews($category,$id){

    
        $this->data['category'] = $category;

        $this->data['news_id'] = $id;

        echo 
            $this->getView('getBlockHeader') .
            $this->getView('getBlockBanner') .
            $this->getView('getBlockpageNews').
            $this->getView('getBlockFooter');

        //
    }

    //метод для отображение новости
    public function categoryPage($category){

    
        $this->data['category'] = $category;

        

        echo 
            $this->getView('getBlockHeader') .
            $this->getView('getBlockBanner') .
            $this->getView('getBlockCategoryPage').
            $this->getView('getBlockFooter');

        //
    }
    public function searchPage(Request $request){
        

        $this->data['page'] = 'Result Search';
        $this->data['searchResult'] = $request->all();

         echo 
            $this->getView('getBlockHeader').
            $this->getView('getBlockBanner').
            $this->getView('getBlockSearchPage').
            $this->getView('getBlockFooter');
  
    }


    public function getView( $v = '' )
    {
        if( method_exists( $this->bl, $v ) )
        {

            $get_v = $this->bl->$v( $this->data );

            if( isset( $get_v['data'] ) )
            {
                // получаю данные которые пришли с другого контроллера. и совмещаю с текущими
                $this->data = array_merge( $this->data, $get_v['data'] );

            }


            // ошибка 404
            if( isset( $get_v['404'] ) ){
                exit(View::make('blocks.errors.404')->render());
            }

            return (isset( $get_v['content'] ) ? $get_v['content'] : '');
        }
    }
}
