<div class="container">
    <!-- Example row of columns -->
    <div class="row">
      @if(isset($news))
        @foreach($news as $item)
          <div class="col-md-4">
        <h2>{{$item->title}}</h2>
        <img src="{{ Voyager::image( $item->image) }}" class="img-fluid">
        <p>{!!$item->discription!!}</p>
         <p>Просмотры:{{$item->views}} || <span>Категория: {{$item->categories->name}} || Дата: {{$item->created_at}}</span></p>
        <p><a class="btn btn-secondary" href="/{{$item->categories->slug}}/{{$item->id}}" role="button">View details &raquo;</a></p>
      </div>
        @endforeach
      @endif
    <nav aria-label="Page navigation example" style="width: 100%;display: flex;justify-content: center; margin-top:100px; margin-bottom:100px">
    {{$news->links()}}
</nav>
</div>
</div>