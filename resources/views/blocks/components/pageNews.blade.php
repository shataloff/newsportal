<div class="blog-post">
  <div class='container'>
            @if(isset($itemNews))
            <h2 class="blog-post-title">{{$itemNews[0]->title}}</h2>
             <img src="{{ Voyager::image( $itemNews[0]->image) }}" class="img-fluid">
            <p class="blog-post-meta">Дата: {{$itemNews[0]->created_at}} || Просмотры: {{$itemNews[0]->views}} || Категория:{{$itemNews[0]->categories->name}} </p>
            <p class="blog-post-meta">{!!$itemNews[0]->discription!!}</p>
            @endif
   </div>       
</div>