@if(isset($lastNews))
  <div class="jumbotron">
    <div class="container">
      <h1 class="display-3">{{$lastNews->title}}</h1>
      <img src="{{ Voyager::image( $lastNews->image) }}" class="img-fluid">
      <p>{!!$lastNews->discription!!}</p>
      <p><span>Категория: {{$lastNews->categories->name}} || Дата: {{$dateFormat}}</span></p>
      <p></p>
      <p><a class="btn btn-primary btn-lg" href="/{{$lastNews->categories->slug}}/{{$lastNews->id}}" role="button">Learn more &raquo;</a></p>
    </div>
  </div>
  @endif