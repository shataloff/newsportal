<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','MainController@index')->name('home');

//роут для отображение новости
Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

//роут для одиночной станицы
Route::get('/{category}/{id}','MainController@pageNews')->name('pageNews');


//роут для отоброжений результата поиска
Route::get('/search','MainController@searchPage')->name('categoryPage');
//роут для отоброжений новостей по категориям
Route::get('/{category}','MainController@categoryPage')->name('categoryPage');
//для обработки ajax
Route::get('/ajax', 'BlocksController@ajax');
Route::post('/ajax', 'BlocksController@ajax');



